<!DOCTYPE html>
<html>
<head>
	<title>EjercicioWeb2</title>
</head>
<body>
	<h2>División de 2 números</h2>
	<form>
		<input type="number" name="dividendo" placeholder="capture dividendo">
		<input type="number" name="divisor" placeholder="capture divisor">
		<button type="submit">Calcular</button>
	</form>
	<?php if(isset($_GET['dividendo']) && isset($_GET['divisor'])): ?>
	<?php 
		$a = $_GET['dividendo'];
		$b = $_GET['divisor'];
	?>
	<ul>
		<li>Resultado = 
		<?php
			$resultado = '';
			// validar que dividendo sea mayor a 0
			if ($b>0) {
				$resultado = $a/$b;
			}
			
			echo $resultado;
		?> 	
		</li>
		<li>Residuo = 
		<?php 
			$residuo = '';
			// validar que dividendo sea mayor a 0
			if ($b>0) {
				$residuo = $a%$b;
				// $residuo = fmod($a, $b);
			}
			
			echo $residuo;
		?> 	
		</li>
		<li>
			Mensaje: El numero <?php echo $a ?? '' ?> 
			<?php echo $residuo===0 ? 'si' : 'no'; ?> 
			es divisible entre el número <?php echo $b ?? '' ?>
		</li>
	</ul>
	<?php endif; ?>
</body>
</html>